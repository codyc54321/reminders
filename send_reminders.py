#!/usr/bin/env python

import datetime

REMINDERS = [
    {'name': 'Rennie', 'event': 'birthday', 'date': datetime.date(1953, 5, 13)},
    {'name': 'mom', 'event': 'birthday', 'date': datetime.date(1953, 6, 24)},
]


def get_date_diff(date_one, date_two):
    diff = date_one - date_two
    return abs(diff.days)


def get_event_date_diff(event_info):
    date = event_info['date']
    date = datetime.date(datetime.date.today().year, date.month, date.day)
    diff = get_date_diff(date, datetime.date.today())
    return diff



diff = get_event_date_diff(REMINDERS[0])
print(diff)
